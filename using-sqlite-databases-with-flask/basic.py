# This file is used to create all the database models
# The database and initial values is created when you run setupdatabase.py
import os
from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate # pip install Flask_Migrate

# DATABSE CREATION CODE START ###################
# __file__ --> gives the file name which is basic.py
# os.path.dirname(__file__) - gives the directory name where the file is
# os.path.abspath - gives the full path of the file which is /home/jeril/Desktop
basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

# this sets up the database location
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+os.path.join(basedir, 'data.sqlite')
# this turns off Flask-SQLAlchemy event system and disables the warning during run time
# SQLALCHEMY_TRACK_MODIFICATIONS can also take huge system resource
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# this creates the database
db = SQLAlchemy(app)

"""
  If you make changes to the database schema in the puppy class like adding another 
  column to the puppy class then the same has to be applied in the database. This can be 
  done using Flask Migrate 
"""

Migrate(app,db)

# Lines 9, 14,17,19 creates a sqlite database for us.
# DATABASE CREATION CODE END ################################

# Each table is created using a class and
# In the class you provide the table name and the columns for the table
class Puppy(db.Model):

    # Provide the table name
    __tablename__ = 'puppies'

    # create columns for the table puppies
    # Id is of type integer and it is the primary key
    # Refer https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    age = db.Column(db.Integer)

    # __init__ is a reserved method in python. It is a constructor which gets automatically called
    # __init__ is usually used to initialize the attributes of a class
    # id is not required to be passed to __init__ method since it will be auto created
    # self refers to the current class name which is Puppy
    def __init__(self,name,age):
        self.name = name
        self.age = age
    # To know what is the use of __repr__ check here - https://www.youtube.com/watch?v=IIzOQhf5AKo
    # __repr_ can create a string representation of the current object.
    # f -> replaces the format method - https://www.youtube.com/watch?v=nghuHvKLhJA
    # f basically replaces the places holders provided {}. This works only above python 3.6
    def __repr__(self):
        return f"Puppy {self.name} is {self.age} years old."
    