# This is an example of crud using database

# imports db and Puppy from basic.py
# db is an object of the database instance
# Puppy is the class defined in basic.py
from basic import db,Puppy

## CREATE ##
# Here you are creating objects of Puppy
# Puppy is a class defined in basic.py and when class is called it calls the constructor __init__
# The constructor __init__ takes two values name and age
# Finally we are storing it in the session variable
my_puppy = Puppy('Rufus', 5)
db.session.add(my_puppy)
db.session.commit()

# Note: All values will be returned in the below format as you have mentioned in __repr__ function inside basic.py
# Puppy {self.name} is {self.age} years old."

## READ ##
# list of puppy objects in the table
all_puppies = Puppy.query.all()
print(all_puppies)

# Select By ID
puppy_one = Puppy.query.get(1)
print(puppy_one.name)

# FILTERS by name
puppy_frankie = Puppy.query.filter_by(name='Frankie')
print(puppy_frankie.all())

# UPDATE
# 1. First grab the primary key of the row that has to be updated
# 2. Then update the corresponding data

first_puppy = Puppy.query.get(1)
first_puppy.age = 10
db.session.add(first_puppy)
db.session.commit()


# Delete
# 1. First grab the primary key of the row that has to be deleted
# 2. Then delete the corresponding data

second_pup = Puppy.query.get(2)
db.session.delete(second_pup)
db.session.commit()


# Now print all the puppies again to check whether delettion and updation has worked

all_puppies = Puppy.query.all()
print(all_puppies)