This application uses SQLite 

# Install Flask-SQLAlchemy

pip install Flask-sqlalchemy
pip install Flask_Migrate
 
1. First run basic.py   - all database configuration is provided here along with table details
2. Then run setupdatabase.py - You create the database along with seeding the database with initial data
3. Then run crud.py - All CRUD implementation is shown here

Note:

To sync the changes to the sqllite database you will have to use Flask Migrate. These are the steps below to sync the changes to the sqlite database. Every time you create a new column in the class you will have to run the below flask migrate commands

If you add another column in the puppy class the same has to implemented using Flask migrate.
To use flask migrate follow the below steps

# For Windows

set FLASK_APP=basic.py
flask db init
flask db migrate -m "created puppy table" # run everytime you make changes to the db class
flask db upgrade # run everytime you make changes to the db class

# For linux

export FLASK_APP=basic.py  # run only once
flask db init    # run only once
flask db migrate -m "created puppy table"  # run everytime you make changes to the db class
flask db upgrade # run everytime you make changes to the db class
