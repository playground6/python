# This file basically works like a seed for the database. 

# imports db and Puppy from basic.py
from basic import db,Puppy

# this creates all the tables mentioned in basic.py
db.create_all()

# Here you are creating objects of Puppy and storing it in sam and frank
# Puppy is a class and when class is called it calls the constructor __init__
# The constructor __init__ takes two values name and age
sam = Puppy('Sammy',3)
frank = Puppy('Frankie', 4)

# printing the properties of class
print(sam.id)
print(frank.id)

db.session.add_all([sam,frank])

db.session.commit()

print(sam.name)
print(frank.name)
