# To demonstrate python FORMS we have the following files
#  python file, 
#  index.html file and 
#  thankyou.html file

# In python file we create the form, 
# in index.html we display the form and 
# in thankyou.html file we display the result the user has submitted

from flask import Flask, render_template, session, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import (StringField, 
                    BooleanField,
                    DateTimeField,
                    RadioField,
                    SelectField,
                    TextField,
                    TextAreaField, 
                    SubmitField)
# For Wtforms there are various validators like DataRequired, Email validation etc
# Below we use this validator which checks whether data is entered                     
from wtforms.validators import DataRequired

app =  Flask(__name__)

# If you are using flask_wft, then the below line is required to avoid CSRF attacks
app.config['SECRET_KEY'] = 'mysecretkey'

class InfoForm(FlaskForm):

  # Create the Form here with the necessary fields. 
  # You can also mention what validation is required for each field
  # validators is a list wherin you can provide multiple validators like DataRequired or Email etc
  breed = StringField('What breed are you?', validators=[DataRequired()])
  neutered = BooleanField("Have you been neutered?")
  # choices for RadioField are a list of tuple pairs which has a value and a label
  mood = RadioField('Please choose your mood:',
                       choices=[('mood_one', 'Happy'),
                                ('mood_two', 'Excited')])
  # SelectField would require a unicode string hence you put 'u' in front of the label
  # chi, bf, fish - are value codes for the corresponding options
  # Chicken, Beef and Fish are what user sees on the website
  food_choice = SelectField(u'Pick your favorite food:', 
                         choices=[('chi', 'Chicken'), 
                                  ('bf', 'Beef'),
                                  ('fish', 'Fish')])
  feedback = TextAreaField()
  submit = SubmitField('Submit')


@app.route('/',methods=['GET','POST'])
def index():

 
  form = InfoForm()
  
  # When user enters the values and submits the form all the values are captured and stored in session variable.
  # This session variable will be available across the application
  if form.validate_on_submit():
    session['breed'] = form.breed.data
    session['neutered'] = form.neutered.data
    session['mood'] = form.mood.data
    session['food'] = form.food_choice.data
    session['feedback'] = form.feedback.data
    # Once the form is submitted redirect to thankyou page
    return redirect(url_for('thankyou'))

  return render_template('index.html', form=form)

@app.route('/thankyou')
def thankyou():
  return render_template('thankyou.html')


if __name__ == '__main__':
  app.run(debug=True)
  


