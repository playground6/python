# Tutorial: https://www.youtube.com/watch?v=HyDACIfdPs0

from flask import Flask

from flask_pymongo import PyMongo

from bson.json_util import dumps

from bson.objectid import ObjectId

from flask import jsonify, request

from werkzeug.security import generate_password_hash, check_password_hash

# Declare the flask application
app = Flask(__name__)


app.secret_key = "secretkey"

# Provide mongodb connection string
app.config['MONGO_URI'] = "mongodb+srv://jeriljose:johnjose@cluster0-jcy8l.mongodb.net/test?retryWrites=true&w=majority"

# Passing flask app to the PyMongo library
mongo = PyMongo(app)

########################CREATE AND STORE VALUES IN THE TABLE#############################
@app.route('/add',methods=['POST'])
def add_user():
    _json = request.json
    _name = _json['name']
    _email = _json['email']
    _password = _json['password']

    # If name and email and password and id is present
    if _name and _email and _password and request.method == 'POST':
        _hashed_password = generate_password_hash(_password)
        # use is the name of the table
        id = mongo.db.user.insert({'name':_name, 'email':_email, 'pwd':_hashed_password})

        resp = jsonify("User add successfully")

        resp.status_code = 200

        return resp

    else:
        return not_found()

#####################DISPLAY ALL THE ROWS IN THE TABLE################

@app.route('/users')
def users():
    # user is the name of the table
    users = mongo.db.user.find()
    """
    Mongo db datas will be in BSON format which is mongodb's native format.
    dumps() is used to convert BSON format to JSON format    
    """
    resp = dumps(users)

    return resp

##########################FILTER BY ID & DISPLAY THE RESULTS####################

@app.route('/user/<id>')
def user(id):
    """
    objectId() is a library imported from bson.objectid. In Mongodb objectId is automatically
    created for each row in the collection to uniquely identify the row. So here the provided
    id /users/<id> will be converted to Mongodb understandable format.
    """
    user = mongo.db.user.find_one({'_id':ObjectId(id)})
    resp = dumps(user)
    return resp

#################DELETE BY ID############################

@app.route('/delete/<id>', methods=['DELETE'])
def delete_user(id):
    mongo.db.user.delete_one({'_id': ObjectId(id)})
    resp = jsonify("User deleted successfully")

    resp.status_code = 200

    return resp

#####################################################

@app.route('/update/<id>', methods=['PUT'])
def update_user(id):
    _id = id
    _json = request.json
    _name = _json['name']
    _email = _json['email']
    _password = _json['pwd']
    
    # If name and email and password and id is present
    if _name and _email and _password and _id and request.method == 'PUT':
        # convert password into hash
        _hashed_password = generate_password_hash(_password)
        # The below line updates the user
        mongo.db.user.update_one({'_id': ObjectId(_id['$oid']) if '$oid' in _id else ObjectId(_id)}, {'$set': {'name': _name, 'email': _email, 'pwd': _hashed_password}})
        resp = jsonify("User updated succesfully")
        resp.status_code = 200

        return resp

    else:
        # Calling the error handler function
        return not_found()

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found' + request.url
    }
    resp = jsonify(message)

    resp.status_code = 404

    return resp

# Run the flask app
# Debug=True will automatically restart the flask server
if __name__ == "__main__":
    app.run(debug=True)
